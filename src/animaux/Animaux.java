package animaux;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class Animaux {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Animal> listeAnimaux = new ArrayList<Animal>();
        
        Animal ma1 = new MammifereAquatique("Dauphin",true,false);
        Animal mt1 = new MammifereTerrestre("Renard",true,"foret");
        Animal ra1 = new ReptileAquatique("Caiman",false,true);
        Animal rt1 = new ReptileTerrestre("Lezard",false,"divers");
        
        listeAnimaux.add(ma1);
        listeAnimaux.add(mt1);
        listeAnimaux.add(ra1);
        listeAnimaux.add(rt1);
        
        int cptReptile = 0;
        int cptTerrestre = 0;
        
        for (int i = 0; i < listeAnimaux.size(); i++) {
            if(listeAnimaux.get(i) instanceof Reptile){
                cptReptile++;
            }
            if(listeAnimaux.get(i) instanceof MammifereTerrestre || listeAnimaux.get(i) instanceof ReptileTerrestre ){
                cptTerrestre++;
            }
        }
        
        System.out.println("Reptiles : "+cptReptile);
        System.out.println("Animaux terrestres : "+cptTerrestre);
        
        if(ma1 instanceof MammifereAquatique){
            System.out.println("mammifere aquatique");
        }
        if(ma1 instanceof Mammifere){
            System.out.println("mammifere");
        }
        if(ma1 instanceof Animal){
            System.out.println("animal");
        }
        if(ma1 instanceof Aquatique){
            System.out.println("aquatique");
        }
        
        Aquatique ma2 = new MammifereAquatique("Tortue",true,false);
        
        System.out.println(ma2.eauDouce());
    }
    
}
