package animaux;

/**
 *
 * @author Alexis
 */
public abstract class Mammifere extends Animal {

    private boolean carnivore;

    public Mammifere(String nom, boolean carnivore) {
        super(nom);
        this.carnivore = carnivore;
    }
}
