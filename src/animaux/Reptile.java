package animaux;

/**
 *
 * @author Alexis
 */
public abstract class Reptile extends Animal {

    private boolean venimeux;

    public Reptile(String nom, boolean venimeux) {
        super(nom);
        this.venimeux = venimeux;
    }

}
