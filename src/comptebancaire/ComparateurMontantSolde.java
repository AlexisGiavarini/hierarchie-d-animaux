package comptebancaire;

import java.util.Comparator;

/**
 *
 * @author Alexis
 */
public class ComparateurMontantSolde implements Comparator<CompteBancaireInitial> {

    @Override
    public int compare(CompteBancaireInitial o1, CompteBancaireInitial o2) {
        return Double.compare(o1.donneSolde(),o2.donneSolde());
    }
    
}
