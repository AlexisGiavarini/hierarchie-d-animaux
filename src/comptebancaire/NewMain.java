package comptebancaire;

import java.util.ArrayList;
import static java.util.Collections.sort;

/**
 *
 * @author Alexis
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Client c1 = new Client("Toto","Titi");
        Client c2 = new Client("Tata","Tutu");
        Client c3 = new Client("Jean","Bon");
        Client c4 = new Client("Aude","Javel");
        Client c5 = new Client("Oussama","fairir");
        
        CompteBancaireInitial cpt1 = new CompteBancaireInitial(100,5,c1);
        CompteBancaireInitial cpt2 = new CompteBancaireInitial(200,3,c2);
        CompteBancaireInitial cpt3 = new CompteBancaireInitial(300,6,c3);
        CompteBancaireInitial cpt4 = new CompteBancaireInitial(400,1,c4);
        CompteBancaireInitial cpt5 = new CompteBancaireInitial(500,7,c5);
        
        ArrayList<CompteBancaireInitial> listeCpt = new ArrayList<CompteBancaireInitial>();
        
        listeCpt.add(cpt1);
        listeCpt.add(cpt2);
        listeCpt.add(cpt3);
        listeCpt.add(cpt4);
        listeCpt.add(cpt5);
        
        //Tri sur les numéro de compte avec l'interface Comparable et la méthode
        //compareTo
        sort(listeCpt);
        
        for (int i = 0; i < listeCpt.size(); i++) {
            System.out.println(listeCpt.get(i).donneNumero());
        }
        
        
        
        
        //solution générique avec l'interface Comparator et la méthode compare
        ComparateurNumeroCompte cpNoCompte = new ComparateurNumeroCompte();
        ComparateurNomDetenteur cpNom = new ComparateurNomDetenteur();
        ComparateurMontantSolde cpMontant = new ComparateurMontantSolde();
        
        sort(listeCpt, cpNoCompte);
        for (int i = 0; i < listeCpt.size(); i++) {
            System.out.println(listeCpt.get(i).donneNumero());
        }
        
        sort(listeCpt, cpNom);
        for (int i = 0; i < listeCpt.size(); i++) {
            System.out.println(listeCpt.get(i).donneDetenteur());
        }
        
        sort(listeCpt, cpMontant);
        for (int i = 0; i < listeCpt.size(); i++) {
            System.out.println(listeCpt.get(i).donneSolde());
        }
    }
    
    
}
