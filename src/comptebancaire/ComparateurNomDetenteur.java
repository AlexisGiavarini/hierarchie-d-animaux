package comptebancaire;

import java.util.Comparator;

/**
 *
 * @author Alexis
 */
public class ComparateurNomDetenteur implements Comparator<CompteBancaireInitial> {

    @Override
    public int compare(CompteBancaireInitial o1, CompteBancaireInitial o2) {
        return o1.donneDetenteur().toString().compareTo(o2.donneDetenteur().toString());
    }
    
}
