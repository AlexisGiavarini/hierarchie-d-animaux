package comptebancaire;

import java.util.Comparator;

/**
 *
 * @author Alexis
 */
public class ComparateurNumeroCompte implements Comparator<CompteBancaireInitial> {

    @Override
    public int compare(CompteBancaireInitial o1, CompteBancaireInitial o2) {
        return Integer.compare(o1.donneNumero(), o2.donneNumero());
    }
    
}
